import * as mongoose from 'mongoose';

export function connectDb(){
    const options = {
        useNewUrlParser: true,
    };

    mongoose.connect("mongodb://127.0.0.1/virtido", options).then(()=>{
        console.log('MongoDB is connected');
    }).catch((err)=>{
        console.log('Mongo connect error:', err);
        connectDb();
    })
}