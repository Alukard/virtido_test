import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

export interface IFavoritesModel extends mongoose.Document {
    id?: string;
    name: string;
    createdAt?: Date;
}

const schema = new Schema({
    id:{
        type: ObjectId
    },
    name: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default : Date.now
    }
});

export const Favorites = mongoose.model<IFavoritesModel>("Favorites", schema, 'favorites');
