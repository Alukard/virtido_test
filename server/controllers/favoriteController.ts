import * as promise from 'bluebird';
import {Request, Response} from "express";
import * as mongoose from 'mongoose';
import {IFavoritesModel, Favorites} from "../models/favorites";
import {ObjectID} from "mongodb";


class FavoriteController {
    public get(req: Request, res: Response) {
        Favorites.find().then((data: IFavoritesModel[]) => {
            return res.json(data);
        }).catch((err) => {
            return res.json([]);
        })
    }

    public save(req: Request, res: Response) {
        let favoriteName: string = req.body.name;

        return Favorites.findOneAndUpdate({name: favoriteName}, {$setOnInsert: {name: favoriteName}}, {upsert: true}).then((data: IFavoritesModel) => {
            return res.json(data);
        }).catch((err) => {
            return res.json([]);
        })
    }

    public delete(req: Request, res: Response) {
        let favoriteId: string = req.params.favoriteId;

        if (!favoriteId) {
            return res.json([]);
        }

        return Favorites.remove({_id: new ObjectID(favoriteId)}).then((data: IFavoritesModel) => {
            return res.json([]);
        }).catch((err) => {
            return res.json([]);
        })
    }
}

export default new FavoriteController();