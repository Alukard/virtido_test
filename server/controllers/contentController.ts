import {Request, Response} from "express";
import * as request from 'request-promise';
import * as promise from 'bluebird';

interface AuthorInterface {
    name: string,
    authtype: string,
    clusterid: string
}

interface PaperInterface {
    result: {
        [id: string]: {
            authors: AuthorInterface[],
            title:string
        }
    }
}

class ContentController {
    public get(req: Request, res: Response) {
        let q = req.query.q;
        let url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=' + q + '&retmode=json&retmax=10';

        if (!q || q.length < 1) {
            return res.json([]);
        }

        request.get(url, {json: true}).then((data) => {
            let ids:string[] = data.esearchresult.idlist || [];

            promise.map(ids, (id:string) => {
                let detailUrl = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&retmode=json&rettype=abstract&id=' + id;

                return request.get(detailUrl, {json: true}).then((response: PaperInterface) => {
                    let result = response.result[id];
                    let authorNames = result.authors.map((author)=>{
                        return author.name;
                    });

                    return {
                        title: result.title,
                        authors: authorNames.join(', ')
                    };
                })
            }, {concurrency: 5}).then((papers) => {
                return res.json(papers);
            })
        }).catch((err) => {
            console.log('get content error:', err);

            return res.json([]);
        })
    }
}

export default new ContentController();