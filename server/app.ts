import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoConnection from './utils/mongoConnection';
import * as fs from 'fs';

// init mongoDB models
const models_path = __dirname + '/models';

fs.readdirSync(models_path).forEach(function (file) {
    if (file.indexOf('.js') > -1 && file.indexOf('.js.map') == -1){
        require(models_path + '/' + file)
    }
});

import FavoriteController from './controllers/favoriteController';
import ContentController from './controllers/contentController';

class App {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();
    }

    private config(): void{
        this.app.use(bodyParser.json());
    }
}

const app = new App().app;
const PORT = 8001;

app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});


app.get('/content', (req, res)=>{ContentController.get(req, res)});

app.get('/favorites', (req, res)=>{FavoriteController.get(req, res)});
app.post('/favorite', (req, res)=>{FavoriteController.save(req, res)});
app.delete('/favorite/:favoriteId', (req, res)=>{FavoriteController.delete(req, res)});


app.use(function (err, req, res, next) {
    console.log('app error:',err);
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.listen(PORT, () => {
    console.log('Express server listening on port ' + PORT);

    mongoConnection.connectDb();
});