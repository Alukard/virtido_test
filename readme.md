// install typescript package global
npm install typescript -g

// install mongoDb

// start server side
cd projectDir/server
npm install
npm start

//start UI part
cd projectDir/ui_app
npm install
npm start