import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            query: '',
            favorites:[]
        };
        this.getData = this.getData.bind(this);
        this.getSavedFavorites = this.getSavedFavorites.bind(this);
        this.removeFavorites = this.removeFavorites.bind(this);
        this.saveFavorites = this.saveFavorites.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.reSearch = this.reSearch.bind(this);

        this.timer = null;
        this.serverUrl = 'http://localhost:8001/';

        this.getData();
        this.getSavedFavorites();
    }

    getSavedFavorites(){
        const self = this;

        axios.get(this.serverUrl + 'favorites').then(response => {
            self.setState({favorites: response.data || []})
        });
    }

    getData() {
        const self = this;

        let params = '?q=' + self.state.query;
        let url = this.serverUrl + 'content' + params;

        axios.get(url).then(response => {
            self.setState({data: response.data || []})
        });
    }

    removeFavorites(id){
        const self = this;

        axios.delete(this.serverUrl + 'favorite/'+id).then(response => {
            return self.getSavedFavorites();
        }).catch((err)=>{
            console.log(err)
        })
    }

    reSearch(name){
        this.setState({query:name});
        this.getData()
    }

    saveFavorites(){
        const self = this;

        axios.post(this.serverUrl + 'favorite', {name: this.state.query}).then(response => {
            return self.getSavedFavorites();
        }).catch((err)=>{
            console.log(err)
        })
    }

    handleChange({target}) {
        this.setState({
            [target.name]: target.value
        }, () => {
            if (this.state.query.length > 1) {
                if (this.timer) {
                    clearTimeout(this.timer);
                }

                this.timer = setTimeout(() => {
                    this.setState({data: []});
                    this.getData();
                }, 500)
            } else if (!this.state.query && !this.state.departure) {
                this.getData();
            }
        });
    }

    render() {
        return (
            <div className="App">
                <div style={{'padding': '50px 0'}}>
                    <div style={{width: '50%', display: 'inline-block'}}>
                        <label htmlFor="query">Query</label>
                        <input type="text" name="query" id="query" placeholder="query" value={this.state.query}
                               onChange={this.handleChange}/>
                        <div style={{display:'inline-block', verticalAlign:'top'}}>
                            <button onClick={this.saveFavorites}>SAVE</button>
                        </div>
                    </div>
                </div>
                <div style={{background: '#bfdef3', 'height': '600px', overflow: 'auto', display:'inline-block', verticalAlign:'top', width:'300px', textAlign:'left', margin:'0 10px 0 0'}}>
                    <label>Saved Items:</label>
                    <table style={{width:'100%'}} id="toRemove">
                        <tbody>
                        {this.state.favorites.map((row) => (
                            <tr>
                                <td>{row.name}</td>
                                <td><button onClick={()=>{this.reSearch(row.name)}}>SEARCH</button></td>
                                <td><button onClick={()=>{this.removeFavorites(row._id)}}>REMOVE</button></td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
                <div style={{background: '#bfdef3', 'height': '600px', overflow: 'auto', display:'inline-block', verticalAlign:'top', width:'800px', textAlign:'left'}}>
                    <label>Search result:</label>
                    <table style={{width:'100%'}} id="toSave">
                        <tbody>
                        {this.state.data.map((item) => (
                            <tr>
                                <td>
                                    <div style={{margin:'0 0 15px', borderBottom:'1px #000000 solid'}}>
                                        <div style={{margin:'0 0 5px'}}>{item.title}</div>
                                        <div style={{fontSize:'12px'}}>{item.authors}</div>
                                    </div>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default App;
